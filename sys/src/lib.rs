// Generated by gir (https://github.com/gtk-rs/gir @ 6fbc68a47551)
// from
// from gir-files (@ 7ebd4478b4a5)
// DO NOT EDIT

#![allow(non_camel_case_types, non_upper_case_globals, non_snake_case)]
#![allow(
    clippy::approx_constant,
    clippy::type_complexity,
    clippy::unreadable_literal,
    clippy::upper_case_acronyms
)]
#![cfg_attr(feature = "dox", feature(doc_cfg))]

#[allow(unused_imports)]
use libc::{
    c_char, c_double, c_float, c_int, c_long, c_short, c_uchar, c_uint, c_ulong, c_ushort, c_void,
    intptr_t, size_t, ssize_t, uintptr_t, FILE,
};

#[allow(unused_imports)]
use glib::{gboolean, gconstpointer, gpointer, GType};

// Enums
pub type TmplError = c_int;
pub const TMPL_ERROR_INVALID_STATE: TmplError = 1;
pub const TMPL_ERROR_TEMPLATE_NOT_FOUND: TmplError = 2;
pub const TMPL_ERROR_CIRCULAR_INCLUDE: TmplError = 3;
pub const TMPL_ERROR_SYNTAX_ERROR: TmplError = 4;
pub const TMPL_ERROR_LEXER_FAILURE: TmplError = 5;
pub const TMPL_ERROR_TYPE_MISMATCH: TmplError = 6;
pub const TMPL_ERROR_INVALID_OP_CODE: TmplError = 7;
pub const TMPL_ERROR_DIVIDE_BY_ZERO: TmplError = 8;
pub const TMPL_ERROR_MISSING_SYMBOL: TmplError = 9;
pub const TMPL_ERROR_SYMBOL_REDEFINED: TmplError = 10;
pub const TMPL_ERROR_NOT_AN_OBJECT: TmplError = 11;
pub const TMPL_ERROR_NULL_POINTER: TmplError = 12;
pub const TMPL_ERROR_NO_SUCH_PROPERTY: TmplError = 13;
pub const TMPL_ERROR_GI_FAILURE: TmplError = 14;
pub const TMPL_ERROR_RUNTIME_ERROR: TmplError = 15;
pub const TMPL_ERROR_NOT_IMPLEMENTED: TmplError = 16;
pub const TMPL_ERROR_NOT_A_VALUE: TmplError = 17;
pub const TMPL_ERROR_NOT_A_FUNCTION: TmplError = 18;

pub type TmplExprBuiltin = c_int;
pub const TMPL_EXPR_BUILTIN_ABS: TmplExprBuiltin = 0;
pub const TMPL_EXPR_BUILTIN_CEIL: TmplExprBuiltin = 1;
pub const TMPL_EXPR_BUILTIN_FLOOR: TmplExprBuiltin = 2;
pub const TMPL_EXPR_BUILTIN_HEX: TmplExprBuiltin = 3;
pub const TMPL_EXPR_BUILTIN_LOG: TmplExprBuiltin = 4;
pub const TMPL_EXPR_BUILTIN_PRINT: TmplExprBuiltin = 5;
pub const TMPL_EXPR_BUILTIN_REPR: TmplExprBuiltin = 6;
pub const TMPL_EXPR_BUILTIN_SQRT: TmplExprBuiltin = 7;
pub const TMPL_EXPR_BUILTIN_TYPEOF: TmplExprBuiltin = 8;
pub const TMPL_EXPR_BUILTIN_ASSERT: TmplExprBuiltin = 9;
pub const TMPL_EXPR_BUILTIN_SIN: TmplExprBuiltin = 10;
pub const TMPL_EXPR_BUILTIN_TAN: TmplExprBuiltin = 11;
pub const TMPL_EXPR_BUILTIN_COS: TmplExprBuiltin = 12;
pub const TMPL_EXPR_BUILTIN_PRINTERR: TmplExprBuiltin = 13;
pub const TMPL_EXPR_BUILTIN_CAST_BYTE: TmplExprBuiltin = 14;
pub const TMPL_EXPR_BUILTIN_CAST_CHAR: TmplExprBuiltin = 15;
pub const TMPL_EXPR_BUILTIN_CAST_I32: TmplExprBuiltin = 16;
pub const TMPL_EXPR_BUILTIN_CAST_U32: TmplExprBuiltin = 17;
pub const TMPL_EXPR_BUILTIN_CAST_I64: TmplExprBuiltin = 18;
pub const TMPL_EXPR_BUILTIN_CAST_U64: TmplExprBuiltin = 19;
pub const TMPL_EXPR_BUILTIN_CAST_FLOAT: TmplExprBuiltin = 20;
pub const TMPL_EXPR_BUILTIN_CAST_DOUBLE: TmplExprBuiltin = 21;
pub const TMPL_EXPR_BUILTIN_CAST_BOOL: TmplExprBuiltin = 22;

pub type TmplExprType = c_int;
pub const TMPL_EXPR_ADD: TmplExprType = 1;
pub const TMPL_EXPR_SUB: TmplExprType = 2;
pub const TMPL_EXPR_MUL: TmplExprType = 3;
pub const TMPL_EXPR_DIV: TmplExprType = 4;
pub const TMPL_EXPR_BOOLEAN: TmplExprType = 5;
pub const TMPL_EXPR_NUMBER: TmplExprType = 6;
pub const TMPL_EXPR_STRING: TmplExprType = 7;
pub const TMPL_EXPR_GT: TmplExprType = 8;
pub const TMPL_EXPR_LT: TmplExprType = 9;
pub const TMPL_EXPR_NE: TmplExprType = 10;
pub const TMPL_EXPR_EQ: TmplExprType = 11;
pub const TMPL_EXPR_GTE: TmplExprType = 12;
pub const TMPL_EXPR_LTE: TmplExprType = 13;
pub const TMPL_EXPR_UNARY_MINUS: TmplExprType = 14;
pub const TMPL_EXPR_STMT_LIST: TmplExprType = 15;
pub const TMPL_EXPR_IF: TmplExprType = 16;
pub const TMPL_EXPR_WHILE: TmplExprType = 17;
pub const TMPL_EXPR_SYMBOL_REF: TmplExprType = 18;
pub const TMPL_EXPR_SYMBOL_ASSIGN: TmplExprType = 19;
pub const TMPL_EXPR_FN_CALL: TmplExprType = 20;
pub const TMPL_EXPR_ANON_FN_CALL: TmplExprType = 21;
pub const TMPL_EXPR_USER_FN_CALL: TmplExprType = 22;
pub const TMPL_EXPR_GETATTR: TmplExprType = 23;
pub const TMPL_EXPR_SETATTR: TmplExprType = 24;
pub const TMPL_EXPR_GI_CALL: TmplExprType = 25;
pub const TMPL_EXPR_REQUIRE: TmplExprType = 26;
pub const TMPL_EXPR_AND: TmplExprType = 27;
pub const TMPL_EXPR_OR: TmplExprType = 28;
pub const TMPL_EXPR_INVERT_BOOLEAN: TmplExprType = 29;
pub const TMPL_EXPR_ARGS: TmplExprType = 30;
pub const TMPL_EXPR_FUNC: TmplExprType = 31;
pub const TMPL_EXPR_NOP: TmplExprType = 32;
pub const TMPL_EXPR_NULL: TmplExprType = 33;

pub type TmplSymbolType = c_int;
pub const TMPL_SYMBOL_EXPR: TmplSymbolType = 0;
pub const TMPL_SYMBOL_VALUE: TmplSymbolType = 1;

// Constants
pub const TMPL_ENABLE_TRACE: c_int = 0;
pub const TMPL_LOG_LEVEL_TRACE: c_int = 1;
pub const TMPL_MAJOR_VERSION: c_int = 3;
pub const TMPL_MICRO_VERSION: c_int = 0;
pub const TMPL_MINOR_VERSION: c_int = 35;
pub const TMPL_VERSION_S: *const c_char = b"3.35.0\0" as *const u8 as *const c_char;

// Unions
#[repr(C)]
pub struct TmplExpr {
    _data: [u8; 0],
    _marker: core::marker::PhantomData<(*mut u8, core::marker::PhantomPinned)>,
}

impl ::std::fmt::Debug for TmplExpr {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("TmplExpr @ {:p}", self)).finish()
    }
}

// Callbacks
pub type TmplScopeResolver = Option<
    unsafe extern "C" fn(*mut TmplScope, *const c_char, *mut *mut TmplSymbol, gpointer) -> gboolean,
>;

// Records
#[repr(C)]
pub struct TmplScope {
    _data: [u8; 0],
    _marker: core::marker::PhantomData<(*mut u8, core::marker::PhantomPinned)>,
}

impl ::std::fmt::Debug for TmplScope {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("TmplScope @ {:p}", self)).finish()
    }
}

#[repr(C)]
pub struct TmplSymbol {
    _data: [u8; 0],
    _marker: core::marker::PhantomData<(*mut u8, core::marker::PhantomPinned)>,
}

impl ::std::fmt::Debug for TmplSymbol {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("TmplSymbol @ {:p}", self)).finish()
    }
}

#[derive(Copy, Clone)]
#[repr(C)]
pub struct TmplTemplateClass {
    pub parent_class: gobject::GObjectClass,
}

impl ::std::fmt::Debug for TmplTemplateClass {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("TmplTemplateClass @ {:p}", self))
            .field("parent_class", &self.parent_class)
            .finish()
    }
}

#[derive(Copy, Clone)]
#[repr(C)]
pub struct TmplTemplateLocatorClass {
    pub parent_instance: gobject::GObjectClass,
    pub locate: Option<
        unsafe extern "C" fn(
            *mut TmplTemplateLocator,
            *const c_char,
            *mut *mut glib::GError,
        ) -> *mut gio::GInputStream,
    >,
}

impl ::std::fmt::Debug for TmplTemplateLocatorClass {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("TmplTemplateLocatorClass @ {:p}", self))
            .field("parent_instance", &self.parent_instance)
            .field("locate", &self.locate)
            .finish()
    }
}

// Classes
#[derive(Copy, Clone)]
#[repr(C)]
pub struct TmplTemplate {
    pub parent_instance: gobject::GObject,
}

impl ::std::fmt::Debug for TmplTemplate {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("TmplTemplate @ {:p}", self))
            .field("parent_instance", &self.parent_instance)
            .finish()
    }
}

#[derive(Copy, Clone)]
#[repr(C)]
pub struct TmplTemplateLocator {
    pub parent_instance: gobject::GObject,
}

impl ::std::fmt::Debug for TmplTemplateLocator {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("TmplTemplateLocator @ {:p}", self))
            .field("parent_instance", &self.parent_instance)
            .finish()
    }
}

#[link(name = "template_glib-1.0")]
extern "C" {

    //=========================================================================
    // TmplError
    //=========================================================================
    pub fn tmpl_error_get_type() -> GType;
    pub fn tmpl_error_quark() -> glib::GQuark;

    //=========================================================================
    // TmplExprBuiltin
    //=========================================================================
    pub fn tmpl_expr_builtin_get_type() -> GType;

    //=========================================================================
    // TmplExprType
    //=========================================================================
    pub fn tmpl_expr_type_get_type() -> GType;

    //=========================================================================
    // TmplSymbolType
    //=========================================================================
    pub fn tmpl_symbol_type_get_type() -> GType;

    //=========================================================================
    // TmplExpr
    //=========================================================================
    pub fn tmpl_expr_get_type() -> GType;
    pub fn tmpl_expr_new_boolean(value: gboolean) -> *mut TmplExpr;
    pub fn tmpl_expr_new_flow(
        type_: TmplExprType,
        condition: *mut TmplExpr,
        primary: *mut TmplExpr,
        secondary: *mut TmplExpr,
    ) -> *mut TmplExpr;
    pub fn tmpl_expr_new_fn_call(builtin: TmplExprBuiltin, param: *mut TmplExpr) -> *mut TmplExpr;
    pub fn tmpl_expr_new_func(
        name: *mut c_char,
        symlist: *mut *mut c_char,
        list: *mut TmplExpr,
    ) -> *mut TmplExpr;
    pub fn tmpl_expr_new_nop() -> *mut TmplExpr;
    pub fn tmpl_expr_new_null() -> *mut TmplExpr;
    pub fn tmpl_expr_new_number(value: c_double) -> *mut TmplExpr;
    pub fn tmpl_expr_new_require(typelib: *const c_char, version: *const c_char) -> *mut TmplExpr;
    pub fn tmpl_expr_new_simple(
        type_: TmplExprType,
        left: *mut TmplExpr,
        right: *mut TmplExpr,
    ) -> *mut TmplExpr;
    #[cfg(any(feature = "v3_36", feature = "dox"))]
    #[cfg_attr(feature = "dox", doc(cfg(feature = "v3_36")))]
    pub fn tmpl_expr_new_stmt_list(stmts: *mut glib::GPtrArray) -> *mut TmplExpr;
    pub fn tmpl_expr_new_string(value: *const c_char, length: ssize_t) -> *mut TmplExpr;
    pub fn tmpl_expr_new_symbol_assign(
        symbol: *const c_char,
        right: *mut TmplExpr,
    ) -> *mut TmplExpr;
    pub fn tmpl_expr_new_symbol_ref(symbol: *const c_char) -> *mut TmplExpr;
    pub fn tmpl_expr_new_user_fn_call(name: *const c_char, param: *mut TmplExpr) -> *mut TmplExpr;
    pub fn tmpl_expr_eval(
        expr: *mut TmplExpr,
        scope: *mut TmplScope,
        return_value: *mut gobject::GValue,
        error: *mut *mut glib::GError,
    ) -> gboolean;
    pub fn tmpl_expr_new_anon_call(func: *mut TmplExpr, params: *mut TmplExpr) -> *mut TmplExpr;
    pub fn tmpl_expr_new_getattr(left: *mut TmplExpr, attr: *const c_char) -> *mut TmplExpr;
    pub fn tmpl_expr_new_gi_call(
        left: *mut TmplExpr,
        name: *const c_char,
        params: *mut TmplExpr,
    ) -> *mut TmplExpr;
    pub fn tmpl_expr_new_invert_boolean(left: *mut TmplExpr) -> *mut TmplExpr;
    pub fn tmpl_expr_new_setattr(
        left: *mut TmplExpr,
        attr: *const c_char,
        right: *mut TmplExpr,
    ) -> *mut TmplExpr;
    pub fn tmpl_expr_ref(expr: *mut TmplExpr) -> *mut TmplExpr;
    pub fn tmpl_expr_unref(expr: *mut TmplExpr);
    pub fn tmpl_expr_from_string(
        str: *const c_char,
        error: *mut *mut glib::GError,
    ) -> *mut TmplExpr;

    //=========================================================================
    // TmplScope
    //=========================================================================
    pub fn tmpl_scope_get_type() -> GType;
    pub fn tmpl_scope_new() -> *mut TmplScope;
    pub fn tmpl_scope_get(self_: *mut TmplScope, name: *const c_char) -> *mut TmplSymbol;
    pub fn tmpl_scope_list_symbols(self_: *mut TmplScope, recursive: gboolean) -> *mut *mut c_char;
    pub fn tmpl_scope_new_with_parent(parent: *mut TmplScope) -> *mut TmplScope;
    pub fn tmpl_scope_peek(self_: *mut TmplScope, name: *const c_char) -> *mut TmplSymbol;
    pub fn tmpl_scope_ref(self_: *mut TmplScope) -> *mut TmplScope;
    pub fn tmpl_scope_require(
        self_: *mut TmplScope,
        namespace_: *const c_char,
        version: *const c_char,
    ) -> gboolean;
    pub fn tmpl_scope_set(self_: *mut TmplScope, name: *const c_char, symbol: *mut TmplSymbol);
    pub fn tmpl_scope_set_boolean(self_: *mut TmplScope, name: *const c_char, value: gboolean);
    pub fn tmpl_scope_set_double(self_: *mut TmplScope, name: *const c_char, value: c_double);
    pub fn tmpl_scope_set_null(self_: *mut TmplScope, name: *const c_char);
    pub fn tmpl_scope_set_object(
        self_: *mut TmplScope,
        name: *const c_char,
        value: *mut gobject::GObject,
    );
    pub fn tmpl_scope_set_resolver(
        self_: *mut TmplScope,
        resolver: TmplScopeResolver,
        user_data: gpointer,
        destroy: glib::GDestroyNotify,
    );
    pub fn tmpl_scope_set_string(self_: *mut TmplScope, name: *const c_char, value: *const c_char);
    pub fn tmpl_scope_set_strv(
        self_: *mut TmplScope,
        name: *const c_char,
        value: *mut *const c_char,
    );
    pub fn tmpl_scope_set_value(
        self_: *mut TmplScope,
        name: *const c_char,
        value: *const gobject::GValue,
    );
    pub fn tmpl_scope_set_variant(
        self_: *mut TmplScope,
        name: *const c_char,
        value: *mut glib::GVariant,
    );
    pub fn tmpl_scope_take(self_: *mut TmplScope, name: *const c_char, symbol: *mut TmplSymbol);
    pub fn tmpl_scope_unref(self_: *mut TmplScope);

    //=========================================================================
    // TmplSymbol
    //=========================================================================
    pub fn tmpl_symbol_get_type() -> GType;
    pub fn tmpl_symbol_new() -> *mut TmplSymbol;
    pub fn tmpl_symbol_assign_boolean(self_: *mut TmplSymbol, v_bool: gboolean);
    pub fn tmpl_symbol_assign_double(self_: *mut TmplSymbol, v_double: c_double);
    pub fn tmpl_symbol_assign_expr(
        self_: *mut TmplSymbol,
        expr: *mut TmplExpr,
        args: *mut glib::GPtrArray,
    );
    pub fn tmpl_symbol_assign_object(self_: *mut TmplSymbol, v_object: *mut gobject::GObject);
    pub fn tmpl_symbol_assign_string(self_: *mut TmplSymbol, v_string: *const c_char);
    pub fn tmpl_symbol_assign_strv(self_: *mut TmplSymbol, strv: *mut *const c_char);
    pub fn tmpl_symbol_assign_value(self_: *mut TmplSymbol, value: *const gobject::GValue);
    pub fn tmpl_symbol_assign_variant(self_: *mut TmplSymbol, v_variant: *mut glib::GVariant);
    pub fn tmpl_symbol_get_boxed(self_: *mut TmplSymbol) -> gpointer;
    pub fn tmpl_symbol_get_expr(
        self_: *mut TmplSymbol,
        params: *mut *mut glib::GPtrArray,
    ) -> *mut TmplExpr;
    pub fn tmpl_symbol_get_symbol_type(self_: *mut TmplSymbol) -> TmplSymbolType;
    pub fn tmpl_symbol_get_value(self_: *mut TmplSymbol, value: *mut gobject::GValue);
    pub fn tmpl_symbol_holds(self_: *mut TmplSymbol, type_: GType) -> gboolean;
    pub fn tmpl_symbol_ref(self_: *mut TmplSymbol) -> *mut TmplSymbol;
    pub fn tmpl_symbol_unref(self_: *mut TmplSymbol);

    //=========================================================================
    // TmplTemplate
    //=========================================================================
    pub fn tmpl_template_get_type() -> GType;
    pub fn tmpl_template_new(locator: *mut TmplTemplateLocator) -> *mut TmplTemplate;
    pub fn tmpl_template_expand(
        self_: *mut TmplTemplate,
        stream: *mut gio::GOutputStream,
        scope: *mut TmplScope,
        cancellable: *mut gio::GCancellable,
        error: *mut *mut glib::GError,
    ) -> gboolean;
    pub fn tmpl_template_expand_string(
        self_: *mut TmplTemplate,
        scope: *mut TmplScope,
        error: *mut *mut glib::GError,
    ) -> *mut c_char;
    pub fn tmpl_template_get_locator(self_: *mut TmplTemplate) -> *mut TmplTemplateLocator;
    pub fn tmpl_template_parse(
        self_: *mut TmplTemplate,
        stream: *mut gio::GInputStream,
        cancellable: *mut gio::GCancellable,
        error: *mut *mut glib::GError,
    ) -> gboolean;
    pub fn tmpl_template_parse_file(
        self_: *mut TmplTemplate,
        file: *mut gio::GFile,
        cancellable: *mut gio::GCancellable,
        error: *mut *mut glib::GError,
    ) -> gboolean;
    pub fn tmpl_template_parse_path(
        self_: *mut TmplTemplate,
        path: *const c_char,
        cancellable: *mut gio::GCancellable,
        error: *mut *mut glib::GError,
    ) -> gboolean;
    pub fn tmpl_template_parse_resource(
        self_: *mut TmplTemplate,
        path: *const c_char,
        cancellable: *mut gio::GCancellable,
        error: *mut *mut glib::GError,
    ) -> gboolean;
    pub fn tmpl_template_parse_string(
        self_: *mut TmplTemplate,
        input: *const c_char,
        error: *mut *mut glib::GError,
    ) -> gboolean;
    pub fn tmpl_template_set_locator(self_: *mut TmplTemplate, locator: *mut TmplTemplateLocator);

    //=========================================================================
    // TmplTemplateLocator
    //=========================================================================
    pub fn tmpl_template_locator_get_type() -> GType;
    pub fn tmpl_template_locator_new() -> *mut TmplTemplateLocator;
    pub fn tmpl_template_locator_append_search_path(
        self_: *mut TmplTemplateLocator,
        path: *const c_char,
    );
    pub fn tmpl_template_locator_get_search_path(
        self_: *mut TmplTemplateLocator,
    ) -> *mut *mut c_char;
    pub fn tmpl_template_locator_locate(
        self_: *mut TmplTemplateLocator,
        path: *const c_char,
        error: *mut *mut glib::GError,
    ) -> *mut gio::GInputStream;
    pub fn tmpl_template_locator_prepend_search_path(
        self_: *mut TmplTemplateLocator,
        path: *const c_char,
    );

}
