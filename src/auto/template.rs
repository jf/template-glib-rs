// This file was generated by gir (https://github.com/gtk-rs/gir)
// from
// from gir-files (https://github.com/gtk-rs/gir-files.git)
// DO NOT EDIT

use crate::Scope;
use crate::TemplateLocator;
use glib::object::Cast;
use glib::object::IsA;
use glib::signal::connect_raw;
use glib::signal::SignalHandlerId;
use glib::translate::*;
use std::boxed::Box as Box_;
use std::fmt;
use std::mem::transmute;
use std::ptr;

glib::wrapper! {
    #[doc(alias = "TmplTemplate")]
    pub struct Template(Object<ffi::TmplTemplate, ffi::TmplTemplateClass>);

    match fn {
        type_ => || ffi::tmpl_template_get_type(),
    }
}

impl Template {
    pub const NONE: Option<&'static Template> = None;

    #[doc(alias = "tmpl_template_new")]
    pub fn new(locator: Option<&impl IsA<TemplateLocator>>) -> Template {
        unsafe {
            from_glib_full(ffi::tmpl_template_new(
                locator.map(|p| p.as_ref()).to_glib_none().0,
            ))
        }
    }
}

pub trait TemplateExt: 'static {
    #[doc(alias = "tmpl_template_expand")]
    fn expand(
        &self,
        stream: &impl IsA<gio::OutputStream>,
        scope: Option<&Scope>,
        cancellable: Option<&impl IsA<gio::Cancellable>>,
    ) -> Result<(), glib::Error>;

    #[doc(alias = "tmpl_template_expand_string")]
    fn expand_string(&self, scope: Option<&Scope>) -> Result<glib::GString, glib::Error>;

    #[doc(alias = "tmpl_template_get_locator")]
    #[doc(alias = "get_locator")]
    fn locator(&self) -> TemplateLocator;

    #[doc(alias = "tmpl_template_parse")]
    fn parse(
        &self,
        stream: &impl IsA<gio::InputStream>,
        cancellable: Option<&impl IsA<gio::Cancellable>>,
    ) -> Result<(), glib::Error>;

    #[doc(alias = "tmpl_template_parse_file")]
    fn parse_file(
        &self,
        file: &impl IsA<gio::File>,
        cancellable: Option<&impl IsA<gio::Cancellable>>,
    ) -> Result<(), glib::Error>;

    #[doc(alias = "tmpl_template_parse_path")]
    fn parse_path(
        &self,
        path: &str,
        cancellable: Option<&impl IsA<gio::Cancellable>>,
    ) -> Result<(), glib::Error>;

    #[doc(alias = "tmpl_template_parse_resource")]
    fn parse_resource(
        &self,
        path: &str,
        cancellable: Option<&impl IsA<gio::Cancellable>>,
    ) -> Result<(), glib::Error>;

    #[doc(alias = "tmpl_template_parse_string")]
    fn parse_string(&self, input: &str) -> Result<(), glib::Error>;

    #[doc(alias = "tmpl_template_set_locator")]
    fn set_locator(&self, locator: &impl IsA<TemplateLocator>);

    #[doc(alias = "locator")]
    fn connect_locator_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId;
}

impl<O: IsA<Template>> TemplateExt for O {
    fn expand(
        &self,
        stream: &impl IsA<gio::OutputStream>,
        scope: Option<&Scope>,
        cancellable: Option<&impl IsA<gio::Cancellable>>,
    ) -> Result<(), glib::Error> {
        unsafe {
            let mut error = ptr::null_mut();
            let is_ok = ffi::tmpl_template_expand(
                self.as_ref().to_glib_none().0,
                stream.as_ref().to_glib_none().0,
                scope.to_glib_none().0,
                cancellable.map(|p| p.as_ref()).to_glib_none().0,
                &mut error,
            );
            assert_eq!(is_ok == glib::ffi::GFALSE, !error.is_null());
            if error.is_null() {
                Ok(())
            } else {
                Err(from_glib_full(error))
            }
        }
    }

    fn expand_string(&self, scope: Option<&Scope>) -> Result<glib::GString, glib::Error> {
        unsafe {
            let mut error = ptr::null_mut();
            let ret = ffi::tmpl_template_expand_string(
                self.as_ref().to_glib_none().0,
                scope.to_glib_none().0,
                &mut error,
            );
            if error.is_null() {
                Ok(from_glib_full(ret))
            } else {
                Err(from_glib_full(error))
            }
        }
    }

    fn locator(&self) -> TemplateLocator {
        unsafe {
            from_glib_none(ffi::tmpl_template_get_locator(
                self.as_ref().to_glib_none().0,
            ))
        }
    }

    fn parse(
        &self,
        stream: &impl IsA<gio::InputStream>,
        cancellable: Option<&impl IsA<gio::Cancellable>>,
    ) -> Result<(), glib::Error> {
        unsafe {
            let mut error = ptr::null_mut();
            let is_ok = ffi::tmpl_template_parse(
                self.as_ref().to_glib_none().0,
                stream.as_ref().to_glib_none().0,
                cancellable.map(|p| p.as_ref()).to_glib_none().0,
                &mut error,
            );
            assert_eq!(is_ok == glib::ffi::GFALSE, !error.is_null());
            if error.is_null() {
                Ok(())
            } else {
                Err(from_glib_full(error))
            }
        }
    }

    fn parse_file(
        &self,
        file: &impl IsA<gio::File>,
        cancellable: Option<&impl IsA<gio::Cancellable>>,
    ) -> Result<(), glib::Error> {
        unsafe {
            let mut error = ptr::null_mut();
            let is_ok = ffi::tmpl_template_parse_file(
                self.as_ref().to_glib_none().0,
                file.as_ref().to_glib_none().0,
                cancellable.map(|p| p.as_ref()).to_glib_none().0,
                &mut error,
            );
            assert_eq!(is_ok == glib::ffi::GFALSE, !error.is_null());
            if error.is_null() {
                Ok(())
            } else {
                Err(from_glib_full(error))
            }
        }
    }

    fn parse_path(
        &self,
        path: &str,
        cancellable: Option<&impl IsA<gio::Cancellable>>,
    ) -> Result<(), glib::Error> {
        unsafe {
            let mut error = ptr::null_mut();
            let is_ok = ffi::tmpl_template_parse_path(
                self.as_ref().to_glib_none().0,
                path.to_glib_none().0,
                cancellable.map(|p| p.as_ref()).to_glib_none().0,
                &mut error,
            );
            assert_eq!(is_ok == glib::ffi::GFALSE, !error.is_null());
            if error.is_null() {
                Ok(())
            } else {
                Err(from_glib_full(error))
            }
        }
    }

    fn parse_resource(
        &self,
        path: &str,
        cancellable: Option<&impl IsA<gio::Cancellable>>,
    ) -> Result<(), glib::Error> {
        unsafe {
            let mut error = ptr::null_mut();
            let is_ok = ffi::tmpl_template_parse_resource(
                self.as_ref().to_glib_none().0,
                path.to_glib_none().0,
                cancellable.map(|p| p.as_ref()).to_glib_none().0,
                &mut error,
            );
            assert_eq!(is_ok == glib::ffi::GFALSE, !error.is_null());
            if error.is_null() {
                Ok(())
            } else {
                Err(from_glib_full(error))
            }
        }
    }

    fn parse_string(&self, input: &str) -> Result<(), glib::Error> {
        unsafe {
            let mut error = ptr::null_mut();
            let is_ok = ffi::tmpl_template_parse_string(
                self.as_ref().to_glib_none().0,
                input.to_glib_none().0,
                &mut error,
            );
            assert_eq!(is_ok == glib::ffi::GFALSE, !error.is_null());
            if error.is_null() {
                Ok(())
            } else {
                Err(from_glib_full(error))
            }
        }
    }

    fn set_locator(&self, locator: &impl IsA<TemplateLocator>) {
        unsafe {
            ffi::tmpl_template_set_locator(
                self.as_ref().to_glib_none().0,
                locator.as_ref().to_glib_none().0,
            );
        }
    }

    fn connect_locator_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe extern "C" fn notify_locator_trampoline<P: IsA<Template>, F: Fn(&P) + 'static>(
            this: *mut ffi::TmplTemplate,
            _param_spec: glib::ffi::gpointer,
            f: glib::ffi::gpointer,
        ) {
            let f: &F = &*(f as *const F);
            f(Template::from_glib_borrow(this).unsafe_cast_ref())
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(
                self.as_ptr() as *mut _,
                b"notify::locator\0".as_ptr() as *const _,
                Some(transmute::<_, unsafe extern "C" fn()>(
                    notify_locator_trampoline::<Self, F> as *const (),
                )),
                Box_::into_raw(f),
            )
        }
    }
}

impl fmt::Display for Template {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.write_str("Template")
    }
}
