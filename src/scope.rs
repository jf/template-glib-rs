use crate::Scope;
use crate::Symbol;
use glib::translate::*;

impl Scope {
    #[doc(alias = "tmpl_scope_set_resolver")]
    pub fn set_resolver<P: Fn(&Scope, &str) -> Option<Symbol> + 'static>(&self, resolver: P) {
        let resolver_data: Box<P> = Box::new(resolver);
        unsafe extern "C" fn resolver_func<P: Fn(&Scope, &str) -> Option<Symbol> + 'static>(
            scope: *mut ffi::TmplScope,
            name: *const libc::c_char,
            symbol: *mut *mut ffi::TmplSymbol,
            user_data: glib::ffi::gpointer,
        ) -> glib::ffi::gboolean {
            let scope = from_glib_borrow(scope);
            let name: Borrowed<glib::GString> = from_glib_borrow(name);
            let callback: &P = &*(user_data as *mut _);
            match (*callback)(&scope, name.as_str()) {
                Some(sym) => {
                    *symbol = sym.to_glib_full();
                    true.into_glib()
                }
                None => false.into_glib(),
            }
        }
        let resolver = Some(resolver_func::<P> as _);
        unsafe extern "C" fn destroy_func<P: Fn(&Scope, &str) -> Option<Symbol> + 'static>(
            data: glib::ffi::gpointer,
        ) {
            let _callback: Box<P> = Box::from_raw(data as *mut _);
        }
        let destroy_call3 = Some(destroy_func::<P> as _);
        let super_callback0: Box<P> = resolver_data;
        unsafe {
            ffi::tmpl_scope_set_resolver(
                self.to_glib_none().0,
                resolver,
                Box::into_raw(super_callback0) as *mut _,
                destroy_call3,
            );
        }
    }
}
