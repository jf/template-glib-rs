use crate::Symbol;
use glib::translate::*;

impl Symbol {
    #[doc(alias = "tmpl_symbol_get_value")]
    #[doc(alias = "get_value")]
    pub fn value(&self) -> glib::Value {
        unsafe {
            let mut value = glib::Value::uninitialized();
            ffi::tmpl_symbol_get_value(self.to_glib_none().0, value.to_glib_none_mut().0);
            value
        }
    }
}
